## Saving cell barcodes that are microglia using a scoring based on the genes that are markers from 
# a clustering script from the 20k subsample (script 1)
# Then subsetting those and a random sample of cells up to 30k.
# Can be used as a reference brain data for label transfer algorithms e.g. SingleCellNEt

library(TENxBrainData)
library(Seurat)
library(SingleCellExperiment)
library(glmGamPoi) # To speed up SCTransform
library(future)
library(patchwork)
library(ggplot2)
options(DelayedArray.block.size=2e9) # Give 2GB memory to single cell experiment
options(future.globals.maxSize= 2097152000) # 2Gb for seurat
plan("multiprocess", workers = 18) # more workers
# check the current active plan
plan()

# Helper function to calculate running time
hms_span <- function(start, end) {
  dsec <- as.numeric(difftime(end, start, unit = "secs"))
  hours <- floor(dsec / 3600)
  minutes <- floor((dsec - 3600 * hours) / 60)
  seconds <- dsec - 3600*hours - 60*minutes
  paste0(
    sapply(c(hours, minutes, seconds), function(x) {
      formatC(x, width = 2, format = "d", flag = "0")
    }), collapse = ":")
}


#### Download data

# Download full data
tenx <- TENxBrainData()
tenx # SingleCellExperiment class
dim(tenx)
counts(tenx)
rowData(tenx)
colData(tenx)
#saving library and mouse information for all cells
write.table(colData(tenx),"1.3M_library_mouse_cell_metadata.txt")

# load list of genes to use as microglia classifier
microglia_enriched_genes = read.table("genes_for_microglial_scoring.txt")
microglia_enriched_genes = microglia_enriched_genes$gene

## slice single cell experiment object and create a seurat object
## filter, normalise, cluster and plot umap with the classifier
# chunk size = 30000 cells
process_1M_cells = function(sce, classifier = microglia_enriched_genes) {
  timeStartWhole = Sys.time() 
  microglia_cell_barcodes = list()
  n = 1
  while (n < dim(sce)[2]) {
    timeStart = Sys.time() 
    if(n+30000 <= dim(sce)[2]){
    sce_subset = sce[, n:(n+30000)]
    message("Working on rows ", n, " through ", n+30000)
    }
    else{
      sce_subset = sce[, n: dim(sce)[2]]
      message("Working on rows ", n, " through ", dim(sce)[2])
    }
    rownames(sce_subset) = as.character(rowData(sce_subset)[, 2])
    # counts(sce_subset, withDimnames=FALSE) <- as(counts(sce_subset, withDimnames = FALSE), "dgCMatrix") # Convert DelayedMatrix to dgCMatrix
    seurat = as.Seurat(sce_subset, data = NULL)
    # Manually adding metadata
    seurat@meta.data$nFeature_RNA = colSums(seurat@assays$RNA@counts > 0) #number of genes: all genes that have expression over 0 count
    seurat@meta.data$nCount_RNA = colSums(seurat@assays$RNA@counts)
    seurat[["percent.mt"]] <-
      PercentageFeatureSet(seurat, pattern = "^mt-")
    
    
    #Filter
    seurat = subset(seurat, subset = nFeature_RNA > 300 & percent.mt < 10)
    message("There are  ", nrow(seurat@meta.data), " cells now")
    
    # Normalise
    seurat = SCTransform(seurat, method = "glmGamPoi", verbose = T)
    
    
    calculate_PCA_UMAP_neighbors_clusters = function(seurat_object){
      timeStart <- Sys.time()
      
      seurat_object <- RunPCA(seurat_object, verbose = FALSE)
      
      seurat_object <- FindNeighbors(seurat_object, dims = 1:30, verbose = FALSE)
      seurat_object <- FindClusters(seurat_object,
                                    verbose = FALSE)
      
      seurat_object <- RunUMAP(seurat_object, dims = 1:30, verbose = FALSE)
      
      timeEnd <- Sys.time() 
      message("Time elapsed calculating PCA, clusters and UMAP = ", hms_span(timeStart, timeEnd))
      
      return(seurat_object)
    }
    
    seurat = calculate_PCA_UMAP_neighbors_clusters(seurat)
    
    ## Scoring cells according to set of microglia markers detected in 20k subsample clustering
    seurat <- AddModuleScore(
      object = seurat,
      features = list(classifier),
      ctrl = 100,
      name = 'microglia_Features'
    )
    
    UMAPS_per_lib = function(seurat_object) {
      timeStart <- Sys.time()
      
      p1 = DimPlot(seurat_object, label = TRUE) + NoLegend() + ggtitle("Clusters")
      p2 =FeaturePlot(seurat_object, label = F, features  = "percent.mt", order = TRUE) + 
        ggtitle("Mit percent") 
      p3 = FeaturePlot(seurat_object, label = F, features  = "nFeature_RNA", order = TRUE) + 
        ggtitle("Number of genes")
      p4 = FeaturePlot(seurat_object, label = F, features  = "microglia_Features1", order = TRUE) + 
        ggtitle("Microglia score")
      p5 = DimPlot(seurat_object, label = F,group.by = "Library") + NoLegend() + ggtitle("Library")
      p6 = DimPlot(seurat_object, label = F,group.by = "Mouse") + ggtitle("Mouse")
      
      p = (p1 | p2) / (p3 | p4) / (p5 | p6)
      
      timeEnd <- Sys.time() 
      message("Time elapsed plotting UMAPS = ", hms_span(timeStart, timeEnd))
      
      return(p)
    }
    
    p = UMAPS_per_lib(seurat)
    
    png( paste0("inspection_1M/UMAP_30k_cells_", n, "_through_", n+30000,".png"), 
         width = 8, height = 12, units = "in", res = 400)
    print(p)
    dev.off()
    ## Select cells with microglia scoring value >= 1
    microglia_cell_barcodes[[n]] = rownames(seurat@meta.data[seurat@meta.data$microglia_Features1>=1,])
    n = n+30001
    
    timeEnd <- Sys.time() 
    message("Time elapsed per chunk = ", hms_span(timeStart, timeEnd))
    gc()
  }
  return(microglia_cell_barcodes)
  timeEndWhole <- Sys.time() 
  message("Time elapsed for all chunks = ", hms_span(timeStartWhole, timeEndWhole))
  
}
microglia_cell_barcodes = process_1M_cells(tenx, classifier = microglia_enriched_genes)
barcode_vector = unlist(microglia_cell_barcodes)

write.table(barcode_vector,"microglia_cell_barcodes.txt",row.names = F, quote = F, col.names = F)
#######################################
## select cells with microglial markers and also a random set of others- save to seurat
barcodes_no_microglia = colnames(tenx[,!(colnames(tenx) %in% barcode_vector)])
# random sample of barcodes
barcodes_sample = sample(barcodes_no_microglia,size = 30000-length(barcode_vector))
length(barcodes_sample) + length(barcode_vector)
barcodes_sample = c(barcodes_sample,barcode_vector)

## Subset selected cell barcodes
tenx_subset = tenx[, barcodes_sample]
dim(tenx_subset)
rm(tenx)





rownames(tenx_subset) = as.character(rowData(tenx_subset)[, 2])
seurat = as.Seurat(tenx_subset, data = NULL)
saveRDS(seurat,"seurat_30K_microglia_enriched_nofilters.rds")
# Manually adding metadata
seurat@meta.data$nFeature_RNA = colSums(seurat@assays$RNA@counts > 0) #number of genes: all genes that have expression over 0 count
seurat@meta.data$nCount_RNA = colSums(seurat@assays$RNA@counts)
seurat[["percent.mt"]] <-
  PercentageFeatureSet(seurat, pattern = "^mt-")



plot1 <- FeatureScatter(seurat, feature1 = "nCount_RNA", feature2 = "percent.mt") + NoLegend()  +
  ylab("% of mitochondrial genes") +
  xlab("UMI counts")
plot2 <- FeatureScatter(seurat, feature1 = "nCount_RNA", feature2 = "nFeature_RNA") + 
  ylab("Number of genes") +
  xlab("UMI counts")


png("inspection_30k_microglia_enriched/metrics_scatter.png",
    width = 7, height = 3, units = "in", res = 400)
plot1 + plot2 
dev.off()

#Filter
seurat = subset(seurat, subset = nFeature_RNA > 300 & percent.mt < 10)
message("There are  ", nrow(seurat@meta.data), " cells now")

# Normalise
seurat = SCTransform(seurat, method = "glmGamPoi", verbose = T)


calculate_PCA_UMAP_neighbors_clusters = function(seurat_object){

  seurat_object <- RunPCA(seurat_object, verbose = FALSE)
  
  seurat_object <- FindNeighbors(seurat_object, dims = 1:30, verbose = FALSE)
  seurat_object <- FindClusters(seurat_object,
                                verbose = FALSE)
  
  seurat_object <- RunUMAP(seurat_object, dims = 1:30, verbose = FALSE)
  
  return(seurat_object)
}

seurat = calculate_PCA_UMAP_neighbors_clusters(seurat)

## Scoring cells according to set of microglia markers detected in 20k subsample clustering
seurat <- AddModuleScore(
  object = seurat,
  features = list(microglia_enriched_genes),
  ctrl = 100,
  name = 'microglia_Features'
)

UMAPS_per_lib = function(seurat_object) {

  p1 = DimPlot(seurat_object, label = TRUE) + NoLegend() + ggtitle("Clusters")
  p2 =FeaturePlot(seurat_object, label = F, features  = "percent.mt", order = TRUE) + 
    ggtitle("Mit percent") 
  p3 = FeaturePlot(seurat_object, label = F, features  = "nFeature_RNA", order = TRUE) + 
    ggtitle("Number of genes")
  p4 = FeaturePlot(seurat_object, label = F, features  = "microglia_Features1", order = TRUE) + 
    ggtitle("Microglia score")
  p5 = DimPlot(seurat_object, label = F,group.by = "Library") + NoLegend() + ggtitle("Library")
  p6 = DimPlot(seurat_object, label = F,group.by = "Mouse") + ggtitle("Mouse")
  
  p = (p1 | p2) / (p3 | p4) / (p5 | p6)
  
  return(p)
}

p = UMAPS_per_lib(seurat)

png( paste0("inspection_30k_microglia_enriched/UMAP_30k_cells.png"), 
     width = 8, height = 12, units = "in", res = 400)
print(p)
dev.off()

saveRDS(seurat,"seurat_30K_microglia_enriched_filtered.rds")
