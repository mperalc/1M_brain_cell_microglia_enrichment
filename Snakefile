
#!/usr/bin/env python3
# Snakefile for running analysis of 10X 1.3M mouse brain cells

# snakemake location:  /software/teamtrynka/conda/trynka-base/bin/snakemake

import os

rule all:
    input:
        file='../duplicates_cell_barcodes.txt'

rule mark_doublets:
    output:
        doublets="../duplicates_cell_barcodes.txt"
    message: "Processing doublets with DoubletFinder. Run with snakemake --jobs 1 --cluster [comma] bsub {params.group} {params.queue} {params.cores} {params.memory} {params.jobname} {params.error} [comma]"
    threads: 5
    params:
        group= "-G teamtrynka",
        queue="-q basement",
        cores="-n 5",
        memory="-M30000 -R'span[hosts=1] select[mem>30000] rusage[mem=30000]'",
        jobname= "-o ../logs/log.mark_doublets.%J.%I",
        error="-e ../errors/error.mark_doublets.%J.%I"
    shell:
        """
        ## R4

        /lustre/scratch118/humgen/resources/conda_envs/R.4/bin/Rscript 2.Mark_duplicates_1M_data.R
        """
